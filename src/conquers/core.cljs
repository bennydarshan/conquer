(ns conquers.core
    (:require
      [ajax.core :refer [GET POST]]
      [reagent.core :as r]))

;; -------------------------
;; Views
(defonce nickname (r/atom ""))
(defonce location (.. js/window -location -href))
;(defonce location "http://localhost:8080/JWS_war_exploded/")
(defonce room-to-join (r/atom ""))
(defonce online-players (r/atom ["moshe" "yacov"]))
(defonce watchers (r/atom []))
(defonce available-rooms (r/atom ["Yacov's Room" "Moshe's Room"]))
(defonce xml-file (r/atom nil))
(defonce in-room (r/atom nil))
(defonce subscriptions (r/atom [+]))
(defonce is-my-turn (r/atom false))
(defonce game-board (r/atom nil))
(defonce total (r/atom 0))
(defonce act-on (r/atom nil))
(defonce error (r/atom ""))
(defonce update-count (r/atom 0))
(defonce debug (r/atom nil))
(defonce action-type (r/atom false))
(defonce text-value (r/atom ""))
(defonce chat-log (r/atom []))
(defonce cycles (r/atom []))
(defonce winner (r/atom [0 0]))

(defonce subscriptions-run (js/setInterval #(doseq [f @subscriptions] (f)) 1000))

(defn get-rooms []
  (GET (str location "rooms") {:response-format :json :keywords? true :handler #(reset! available-rooms %)}))

(defn get-watchers []
  (GET (str location "roomwatchers") {:response-format :json :keywords? true :params {:room (:roomId @in-room)} :handler #(reset! watchers %)}))

(defn get-update []
  (GET (str location "update") {:response-format :json :keywords? true :params {:room (:roomId @in-room) :update 0} :handler #(do (reset! update-count (:update %))
                                                                                  (reset! cycles [(:cycle %) (:cycles %)])
                                                                                  (reset! winner [(:winner %) (:profit %) ])
                                                                                  (swap! game-board assoc :territories (:territories %)))}))


(defn get-players [url]
  (GET (str location url) {:response-format :json :keywords? true :handler #(reset! online-players (:onlineUserPool %))}))

(defn get-players-in-room []
  (GET (str location "roomplayers") {:response-format :json :params {:room (:roomId @in-room)} :keywords? true :handler #(do (reset! online-players %)
                                                                                                                              (reset! total (:turings (first (filter (fn [x] (= (:nick @nickname) (:name x))) %)))))}))

(defn myturn? []
  (GET (str location "playerturn"){:response-format :json :keywords? true :params {:room (:roomId @in-room)} 
                             :handler #(reset! is-my-turn (:player %))}))

(defn get-chat []
  (GET (str location "chat") {:response-format :json :keywords? true :params {:room (:roomId @in-room)} :handler #(reset! chat-log (map (fn [message] (str (:name message) ": " (:message message))) %)) }))

(defn login-page [] 
 (let [nick (r/atom "")]
  (fn [] 
  [:div
   ; (str "You are at " location)
    [:h1 "Please enter your nickname:"]
    [:br]
    [:input {:type "text" :value @nick :on-change #(reset! nick (.. % -target -value))}]
    [:input {:type "button" :value "Login" :on-click ;#(reset! nickname @nick)}]
    #(POST (str location "login") {:response-format :json :format :json :keywords? true :params {:name @nick}
                                                                                    :handler (fn [reply] (reset! nickname {:id (:id reply) :nick @nick}))
                                                                                    })}]
  ])
 )
)

(defn new-game []
 (let [file (r/atom nil)
       text (r/atom "")
       freader (js/FileReader.)
       freader-more (set! (.-onload freader) #(reset! text (-> % .-target .-result)))]
  (fn []
    [:do
     (if (not= @file nil)
       [:input {:type "file" :accept ".xml,Xml file" :on-change #(.readAsText freader (-> % .-target .-files (aget 0)))}]
       )
     [:input {:type "button" :value "New Game" :on-click #(if (= @file nil) (do (reset! file "") (reset! error "")) (POST (str location "newroom")
                                                                                               {:format :json :params {:id (:id @nickname) :data @text}
                                                                                               :handler (fn [reply] (do (reset! file nil) (reset! error "")))
                                                                                               :error-handler (fn [{:keys [status reply]}] (do (reset! error "Duplicate entry")(reset! file nil)))})
                                                                                               )}]
     ])
 )
)

(defn join-game []
  [:input {:type "button" 
           :value (if (= @room-to-join "") "Choose a room to join" (str "Join " (:roomTitle @room-to-join)))
           :on-click #(when (not= @room-to-join "") ;(reset! in-room true) (reset! room-to-join "")
           (POST (str location "joinroom") {
                                            :format :json :params {:id (:id @nickname) :room (:roomId @room-to-join)}
                                            :handler (fn [reply] (do (reset! in-room @room-to-join) (reset! room-to-join nil)))
                                            :error-handler (fn [{:keys [status reply]}] (reset! error "Server is full"))}
           ))
  }]
)

(defn rooms-page []
  (do 
    (reset! subscriptions [get-rooms (partial get-players "onlineusers")])
  [:div 
    [:h3 "Your id is " (str (:id @nickname))]
    ;(str @subscriptions)
    ;(str @online-players)
    [:table 
    [:tbody
      [:tr
        [:td
          [:ul
            (for [room @available-rooms]
              [:li
                [:label {:on-click #(reset! room-to-join room)} (:roomTitle room) (str " [" (:inRoom room) "/" (:roomSize room) "]")] [:br]
                [:small (str "creator: " (:username (:roomCreator room)) )][:br]
                [:small (if (:isActive room) "Room is Active" "Room is inActive")]
              ]
            )
          ]

        ]
        [:td
          [:ul
            (for [player @online-players]
              [:li player]) 
          ]
        ]
      ]
      [:tr
        [:td
          (str @in-room)
          [new-game]
        ]
        [:td
          [join-game]
          [:input {:type "button" :value "Logout" :on-click #(reset! nickname "")}]
        ]
    ][:tr [:td @error]]]
  ]])
)

(defn take-action [action & prices]
  (let [data (r/atom (vec (map #(+ 0) prices)))
        clicked-territory (get (vec (:territories @game-board)) (dec @act-on))]

  (fn [] 
 [:div  
    (str @total)
    (doall (for [p (range (count prices))]
      (let [maximum (quot (- (+ @total (* (get (vec prices) p) (get (vec @data) p))) (reduce + (map * prices @data))) (get (vec prices) p))]
      [:div [:input {:key p :type "range" :min 0 :max maximum :value (get @data p) :on-change #(swap! data assoc p (int (.. % -target -value ))) }](str " " (get @data p) " / " maximum)[:br]])))
      [:input {:type "button" :value "Act!" :disabled (>= (:threshold clicked-territory) (reduce + (map * @data (map :initfp (:units @game-board))))) 
                                                          :on-click #(POST (str location "action") {:response-format :json :format :json :keywords? true 
                                                          :params {:room (:roomId @in-room) :tile @act-on :action action :id (:id @nickname) :army @data}
                                                          :handler (fn [] (reset! act-on nil))
                                                          })}]
      ])))


(defn game-map []
  (doall (defonce map-init! (GET (str location "gamedata") {:response-format :json :keywords? true :params {:room (:roomId @in-room)} :handler #(do (reset! game-board %)
                                                                                                                                                     (reset! total (:initfunds %)))}))
  (->>
    (:territories @game-board)
    (map #(vector :label 
      {:on-click (fn [] (GET (str location "conquerable") {:response-format :json :keywords? true :params {:room (:roomId @in-room) :user (:id @nickname)}
                                                           :handler (fn [reply] (if (some (fn [x] (= x (:id %))) reply) (reset! act-on (:id %))(reset! act-on nil)))}))} ;reset! act-on (:id %)
      [:ul [:li "id: " (:id %)] [:li "threshold: " (:threshold %)] [:li "profit: " (:profit %)] [:li "occupied by: " (:occupant %)]]))
    (map #(vector :td  %))
    (partition (:horizontal (:board @game-board)))
    (map #(apply vector :tr %))
    )))

(defn take-action-proxy []
(let [prices (map :price (:units @game-board))
      clicked-territory (get (vec (:territories @game-board)) (dec @act-on))]
(when (and (= @is-my-turn (:id @nickname)) (= @winner [0 0]))
[:div
[:input {:type "button" :value "Pass!" :on-click #(POST (str location "action") {:format :json :keywords? true :params {:id (:id @nickname) :room (:roomId @in-room) :action -1 :tile 0}})}]

(cond 
  (= @act-on nil) nil
  (= (:occupant clicked-territory) (:nick @nickname)) [:div [:input {:type "button" :on-click #(POST (str location "action") {:format :json :params {:tile @act-on :id (:id @nickname) :room (:roomId @in-room) :army [0] :action 1}} :handler (fn [] (reset! act-on nil))) :value "Restore" :disabled (not (:restore clicked-territory))}] (apply vector take-action 0 prices)]
  (= (:occupant clicked-territory) "none") (apply vector take-action 0 prices)
  :else [:div [:input {:type "radio" :checked (not @action-type) :on-change #(reset! action-type (not %)) } ] [:label {:on-click #(reset! action-type false)} "Well-Planned"][:br][:input {:type "radio" :on-change #(reset! action-type %) :checked @action-type}][:label {:on-click #(reset! action-type true)} "Feeling-Lucky"] (apply vector take-action (if @action-type 0 1) prices)])])))

(defn game-page []
  (do (reset! subscriptions [get-players-in-room myturn? get-update get-chat get-watchers])
  [:table [:tbody
    [:tr [:td [:h2 (:title @game-board)]
              (when (not= [0 0] @winner) [:h3 "The winner is " (:name (first (filter #(= (:uid %) (first @winner)) @online-players))) "! with a profit of " (second @winner)])
              ]]
    [:tr
      [:td ;; Map
        ;(str (:territories @game-board))
        [:table [:tbody (game-map)]]
       ]
      [:td ;;Players
        [:small (str "cycle " (first @cycles) " out of " (second @cycles))]
        [:ul 

          [:h4 "Players"]
          (for [player @online-players]
            [:li (:name player) (when (= (:plid player) @is-my-turn) "@") (str " " (:turings player) "$") ]) 
          (when (and (not (some #(= (:nick @nickname) (:name %)) @online-players)) (not (:isActive @in-room))) [:input {:type "button" :value "Click to Join" :on-click #(POST (str location "joingame") {:format :json :params {:id (:id @nickname) :room (:roomId @in-room)}})}] )
          [:h4 "Spectators"]
          (for [watcher @watchers] [:li (:username watcher)])]
      ]
    ]
    [:tr
      [:td
        (for [message @chat-log] [:div message [:br]])
        [:br]
        [:input {:type "text" :value @text-value :on-change #(reset! text-value (.. % -target -value))}]
        [:input {:type "button" :value "Send!" :on-click #(POST (str location "chat"){:format :json :keywords? true :params {:message @text-value :room (:roomId @in-room) :user (:id @nickname)}
                                                                                      :handler (fn [] (reset! text-value ""))})}]
      ]
      [:td
        [take-action-proxy]
      ]
    ]
    [:tr
      [:td 
        [:input {:type "button" :value "Leave" :on-click #(POST (str location "leave") {:format :json :keywords? true :params {:user (:id @nickname) :room (:roomId @in-room)}
                                                                                        :handler (fn [] (reset! in-room nil))})}]
      ] 
    ]
  ]])
)

(defn home-page []
 (cond
  (not= @in-room nil) [game-page]
  (not= @nickname "") [rooms-page]

  :else [login-page]
  
 )
)

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))
